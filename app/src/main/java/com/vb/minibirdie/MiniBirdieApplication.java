package com.vb.minibirdie;

import android.app.Application;

import com.vb.minibirdie.db.AppDatabase;


public class MiniBirdieApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.init(this);
    }
}
