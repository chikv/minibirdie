package com.vb.minibirdie.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.vb.minibirdie.db.dao.TweetDao;
import com.vb.minibirdie.db.dao.UserDao;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.model.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Database(entities = {Tweet.class, User.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;
    private Set<Long> cachedIds = new HashSet<>();

    public static synchronized AppDatabase getInstance() {
        if(instance == null){
            throw new IllegalStateException("You need initialize db first");
        } else {
            return instance;
        }
    }

    public  static synchronized AppDatabase init(Context context){
        instance = Room.databaseBuilder(context,
                AppDatabase.class, "minibirdiedb").allowMainThreadQueries().build();
        return instance;
    }

    public void unfavoriteTweet(Tweet tweet){
        tweetDao().delete(tweet);
        cachedIds.remove(tweet.getId());
    }

    public void favoriteTweet(Tweet tweet){
        tweetDao().insert(tweet);
        cachedIds.add(tweet.getId());
        tweet.setFavorite(true);
    }

    public List<Tweet> getFavorites(){
        List<Tweet> favs = tweetDao().getFavorites();
        for(Tweet tweet : favs){
            cachedIds.add(tweet.getId());
        }
        return favs;
    }

    public abstract TweetDao tweetDao();


    public boolean isTweetFavorite(long id){
        return cachedIds.contains(id);
    }

    public void applyFavoritesFlag(List<Tweet> tweets) {
        for(Tweet tweet : tweets) {
            if(cachedIds.contains(tweet.getId())){
                tweet.setFavorite(true);
            } else {
                tweet.setFavorite(false);
            }
        }
    }

    public void initCache(){
        cachedIds.addAll(tweetDao().getFavoritesIds());
    }
}


