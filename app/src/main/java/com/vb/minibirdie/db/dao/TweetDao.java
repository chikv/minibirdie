package com.vb.minibirdie.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vb.minibirdie.model.Tweet;

import java.util.List;

@Dao
public interface TweetDao {

    @Query("SELECT * FROM tweets")
    List<Tweet> getFavorites();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Tweet... tweets);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Tweet tweet);

    @Delete
    void delete(Tweet tweet);

    @Query("DELETE FROM tweets WHERE id = :id")
    void delete(long id);

    @Query("SELECT id FROM tweets")
    List<Long> getFavoritesIds();

}
