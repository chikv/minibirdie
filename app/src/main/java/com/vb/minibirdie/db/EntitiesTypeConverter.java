package com.vb.minibirdie.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vb.minibirdie.model.TweetEntititesWrapper;

import java.lang.reflect.Type;

public class EntitiesTypeConverter {

    @TypeConverter
    public String fromEntitiesWrapper(TweetEntititesWrapper entitiesWrapper) {
        if (entitiesWrapper == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<TweetEntititesWrapper>() {
        }.getType();
        String json = gson.toJson(entitiesWrapper, type);
        return json;
    }

    @TypeConverter
    public TweetEntititesWrapper toEntitiesWrapper(String entitiesWrapperString) {
        if (entitiesWrapperString == null) {
            return new TweetEntititesWrapper();
        }
        Gson gson = new Gson();
        Type type = new TypeToken<TweetEntititesWrapper>() {
        }.getType();
        TweetEntititesWrapper entitiesWrapper = gson.fromJson(entitiesWrapperString, type);
        return entitiesWrapper;
    }

}
