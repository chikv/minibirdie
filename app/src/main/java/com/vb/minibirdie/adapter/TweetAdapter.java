package com.vb.minibirdie.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vb.minibirdie.R;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.model.TweetEntity;

import java.util.ArrayList;
import java.util.List;

public class TweetAdapter extends RecyclerView.Adapter {

    private static final String TAG = "TweetAdapter";
    public static final String EXTRA_TWEET_ID = "EXTRA_TWEET_ID";
    private final int VIEW_TWEET = 0;
    private final int VIEW_LOADMORE = 1;

    public static final String ACTION_FAVORITE = "tweetfav";
    public static final String ACTION_UNFAVORITE = "tweetunfav";

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public interface TweetInteractionListener {
        void entityClick(TweetEntity text);
        void favoriteTweet(Tweet tweet);
        void unfavoriteTweet(Tweet tweet);
    }

    private TweetInteractionListener tweetInteractionListener;

    public void setTweetInteractionListener(TweetInteractionListener tweetInteractionListener) {
        this.tweetInteractionListener = tweetInteractionListener;
    }

    private ArrayList<Tweet> items = new ArrayList<>();

    private LocalBroadcastManager broadcastManager;

    private View.OnClickListener favButtonListener;

    public TweetAdapter(final ArrayList<Tweet> tweets, RecyclerView recyclerView) {
        items = tweets;
        broadcastManager = LocalBroadcastManager.getInstance(recyclerView.getContext());
        favButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "fav button clicked!");
                Object tag = v.getTag();
                if(tag instanceof Tweet){
                    Tweet tweet = (Tweet) tag;
                    Intent i = new Intent();
                    i.setAction(tweet.isFavorite() ? ACTION_UNFAVORITE : ACTION_FAVORITE);
                    i.putExtra(EXTRA_TWEET_ID, tweet.getId());
                    if(tweet.isFavorite()) {
                        tweetInteractionListener.unfavoriteTweet(tweet);
                    } else {
                        tweetInteractionListener.favoriteTweet(tweet);
                    }
                    broadcastManager.sendBroadcast(i);
                }
            }
        };

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.loadMoreItems();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_TWEET : VIEW_LOADMORE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_TWEET) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.tweet_recycler_item, parent, false);

            vh = new TweetViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.loadmore, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TweetViewHolder) {

            Tweet tweet= items.get(position);

            TweetViewHolder tweetHolder = (TweetViewHolder) holder;
            CharSequence spannableTweetText = getSpannableTweetText(tweet);

            tweetHolder.tweettext.setText(spannableTweetText);
            tweetHolder.tweettext.setMovementMethod(LinkMovementMethod.getInstance());

            tweetHolder.username.setText(tweet.getUser().getName());
            tweetHolder.screename.setText(String.format("@%s", tweet.getUser().getScreenName()));
            Glide.with(tweetHolder.itemView)
                    .load(tweet.getUser().getAvatarUrl())
            .apply(RequestOptions.circleCropTransform())
                    .into(tweetHolder.avatar);
            tweetHolder.favButton.setTag(tweet);
            tweetHolder.favButton.setOnClickListener(favButtonListener);
            tweetHolder.favButton.setImageResource(tweet.isFavorite() ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star_big_off);


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    private class EntityClickableSpan extends ClickableSpan{

        private TweetEntity entity;

        public EntityClickableSpan(TweetEntity entity) {
            this.entity = entity;
        }

        @Override
        public void onClick(View widget) {
            Log.d(TAG, "Entitiy clicked!");
            if(tweetInteractionListener != null){
                tweetInteractionListener.entityClick(entity);
            }
        }
    }

    @NonNull
    private CharSequence getSpannableTweetText(Tweet tweet) {
        SpannableStringBuilder builder = new SpannableStringBuilder(tweet.getText());

        List<TweetEntity> entities = tweet.getEntities().getAllEntities();


        if(entities != null){
            for(TweetEntity entity : entities) {
                builder.setSpan(new EntityClickableSpan(entity), entity.getEntityStart(), entity.getEntityEnd(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.setSpan(new ForegroundColorSpan(Color.RED), entity.getEntityStart(), entity.getEntityEnd(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        return builder;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TweetViewHolder extends RecyclerView.ViewHolder {

        public ImageView avatar;
        public TextView username;
        public TextView screename;
        public TextView tweettext;
        public ImageButton favButton;

        public TweetViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.avatar);
            username = itemView.findViewById(R.id.username);
            screename = itemView.findViewById(R.id.screenname);
            tweettext = itemView.findViewById(R.id.tweetText);
            favButton = itemView.findViewById(R.id.favButton);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress);
        }
    }
}
