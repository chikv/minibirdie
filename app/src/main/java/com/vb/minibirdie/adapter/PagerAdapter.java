package com.vb.minibirdie.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.vb.minibirdie.fragment.FavoritesFragment;
import com.vb.minibirdie.fragment.NearbyFragment;
import com.vb.minibirdie.fragment.SearchFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    private Fragment mCurrentFragment;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new SearchFragment();
            case 1:
                return new NearbyFragment();
            case 2:
                return new FavoritesFragment();

        }
        return null;
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
