package com.vb.minibirdie.adapter;

public interface OnLoadMoreListener {
    void loadMoreItems();
}
