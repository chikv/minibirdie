package com.vb.minibirdie.network;

import com.vb.minibirdie.BuildConfig;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkController {

    private static NetworkController instance;

    private String credentials = Credentials.basic(BuildConfig.CONSUMER_KEY, BuildConfig.CONSUMER_SECRET);
    private Retrofit retrofit;
    private TwitterService service;
    private OAuthToken token;

    private NetworkController(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest.newBuilder().header("Authorization",
                        token != null ? token.getAuthorization() : credentials);

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(TwitterService.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(TwitterService.class);
    }



    public static NetworkController getInstance() {
        if(instance == null){
            instance = new NetworkController();
        }
        return instance;
    }

    public String getCredentials() {
        return credentials;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public TwitterService getService() {
        return service;
    }

    public OAuthToken getToken() {
        return token;
    }

    public void setToken(OAuthToken token) {
        this.token = token;
    }
}
