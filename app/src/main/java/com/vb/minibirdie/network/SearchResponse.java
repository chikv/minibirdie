package com.vb.minibirdie.network;

import com.google.gson.annotations.SerializedName;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.network.SearchMetadata;

import java.util.List;

public class SearchResponse {

    @SerializedName("statuses")
    List<Tweet> statuses;

    @SerializedName("search_metadata")
    SearchMetadata searchMetadata;

    public List<Tweet> getStatuses() {
        return statuses;
    }

    public SearchMetadata getSearchMetadata() {
        return searchMetadata;
    }
}
