package com.vb.minibirdie.network;

import com.google.gson.annotations.SerializedName;

public class SearchMetadata {
    @SerializedName("completed_in")
    private double completedIn;
    @SerializedName("max_id")
    private long maxId;
    @SerializedName("next_results")
    private String nexResults;
    @SerializedName("query")
    private String query;
    @SerializedName("count")
    private long count;
    @SerializedName("since_id")
    private long sinceId;

    public double getCompletedIn() {
        return completedIn;
    }

    public void setCompletedIn(double completedIn) {
        this.completedIn = completedIn;
    }

    public long getMaxId() {
        return maxId;
    }

    public void setMaxId(long maxId) {
        this.maxId = maxId;
    }

    public String getNexResults() {
        return nexResults;
    }

    public void setNexResults(String nexResults) {
        this.nexResults = nexResults;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getSinceId() {
        return sinceId;
    }

    public void setSinceId(long sinceId) {
        this.sinceId = sinceId;
    }
}
