package com.vb.minibirdie.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TwitterService {

    String BASE_URL = "https://api.twitter.com/";


    @FormUrlEncoded
    @POST("oauth2/token")
    Call<OAuthToken> postCredentials(@Field("grant_type") String grantType);

    @GET("1.1/search/tweets.json")
    Call<SearchResponse> search(@Query("q") String searchQuery, @Query("count") long count);
    //Call<List<Tweet>> search(@Query("q") String searchQuery, @Query("since_id") long sinceId);
    @GET("1.1/search/tweets.json")
    Call<SearchResponse> search(@Query("q") String searchQuery, @Query("count") long count, @Query("maxId_id") long maxId);
    @GET("1.1/search/tweets.json")
    Call<SearchResponse> searchSince(@Query("q") String searchQuery, @Query("count") long count, @Query("since_id") long sinceId);
    @GET("1.1/search/tweets.json")
    Call<SearchResponse> searchNaearby(@Query("count") long count, @Query("geocode") String location);
    @GET("1.1/search/tweets.json")
    Call<SearchResponse> searchNaearby(@Query("count") long count, @Query("geocode") String location, @Query("since_id") long sinceId);
    @GET("1.1/search/tweets.json")
    Call<SearchResponse> searchNaearbyAfter(@Query("count") long count, @Query("geocode") String location, @Query("max_id") long maxId);

}
