package com.vb.minibirdie.model;

import com.google.gson.annotations.SerializedName;

public class MentionEntity extends TweetEntity {

    private String name;
    @SerializedName("screen_name")
    private String screenName;
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
