package com.vb.minibirdie.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class User {
    @PrimaryKey
    private long id;
    @SerializedName("name")
    private String name;
    @SerializedName("screen_name")
    private String screenName;

    public User(long id, String name, String screenName, String avatarUrl) {
        this.id = id;
        this.name = name;
        this.screenName = screenName;
        this.avatarUrl = avatarUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @SerializedName("profile_image_url")
    private String avatarUrl;//profile_image_url

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
