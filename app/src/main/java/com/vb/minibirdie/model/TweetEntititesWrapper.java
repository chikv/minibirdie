package com.vb.minibirdie.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TweetEntititesWrapper {
    @SerializedName("user_mentions")
    private List<MentionEntity> mentions;
    private List<HashtagEntity> hashtags;
    private List<UrlEntity> urls;

    public List<MentionEntity> getMentions() {
        return mentions;
    }

    public List<HashtagEntity> getHashtags() {
        return hashtags;
    }

    public List<UrlEntity> getUrls() {
        return urls;
    }

    public List<TweetEntity> getAllEntities(){
        List<TweetEntity> allEntitites = new ArrayList<>();
        if(mentions != null){
            allEntitites.addAll(mentions);
        }if(hashtags != null){
            allEntitites.addAll(hashtags);
        }if(urls != null){
            allEntitites.addAll(urls);
        }
        return allEntitites;
    }
}
