package com.vb.minibirdie.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.RoomWarnings;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.vb.minibirdie.db.EntitiesTypeConverter;

@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@Entity(tableName = "tweets")
public class Tweet {
    @PrimaryKey
    private long id;
    private String text;
    @Embedded(prefix = "user_")
    private User user;
    @TypeConverters(EntitiesTypeConverter.class)
    private TweetEntititesWrapper entities;
    @Ignore
    @Expose(deserialize = false, serialize = false)
    private boolean isFavorite = false;

    public Tweet(long id, String text, User user) {
        this.id = id;
        this.text = text;
        this.user = user;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TweetEntititesWrapper getEntities() {
        return entities;
    }

    public void setEntities(TweetEntititesWrapper entities) {
        this.entities = entities;
    }

}
