package com.vb.minibirdie.model;

import com.google.gson.annotations.SerializedName;

public class UrlEntity extends TweetEntity {

    private String url;
    @SerializedName("display_url")
    private String displayUrl;
    @SerializedName("expanded_url")
    private String expandedUrl;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDisplayUrl() {
        return displayUrl;
    }

    public void setDisplayUrl(String displayUrl) {
        this.displayUrl = displayUrl;
    }

    public String getExpandedUrl() {
        return expandedUrl;
    }

    public void setExpandedUrl(String expandedUrl) {
        this.expandedUrl = expandedUrl;
    }
}
