package com.vb.minibirdie.model;

public class TweetEntity {
    public TweetEntity() {
    }

    private int[] indices;

    public int[] getIndices() {
        return indices;
    }

    public void setIndices(int[] indices) {
        this.indices = indices;
    }

    public int getEntityStart(){
        return indices != null ? indices[0] : -1;
    }

    public int getEntityEnd(){
        return indices != null ? indices[1] : -1;
    }

}
