package com.vb.minibirdie;

import android.app.ProgressDialog;
import android.app.usage.NetworkStatsManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.vb.minibirdie.adapter.PagerAdapter;
import com.vb.minibirdie.fragment.FragmentInteractionListener;
import com.vb.minibirdie.fragment.SearchFragment;
import com.vb.minibirdie.network.NetworkController;
import com.vb.minibirdie.network.OAuthToken;
import com.vb.minibirdie.network.TwitterService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentInteractionListener {

    private Retrofit retrofit;
    private TwitterService service;
    private ViewPager pager;
    private NavigationView navigationView;
    private NetworkController nc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        pager = findViewById(R.id.viewpager);
        pager.setOffscreenPageLimit(2);
        pager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        navigationView.setCheckedItem(R.id.nav_search);
                        break;
                    case 1:
                        navigationView.setCheckedItem(R.id.nav_nearby);
                        break;
                    case 2:
                        navigationView.setCheckedItem(R.id.nav_favorites);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        nc = NetworkController.getInstance();
        retrofit = nc.getRetrofit();
        service = nc.getService();
    }

    private ProgressDialog progres;

    @Override
    protected void onStart() {
        super.onStart();
        if(progres == null) {
            progres = new ProgressDialog(this);
            progres.setIndeterminate(true);
            progres.setTitle("Authenticating...");

        }

        if(nc.getToken() == null){
            progres.show();
            service.postCredentials("client_credentials").enqueue(new Callback<OAuthToken>() {
                @Override
                public void onResponse(Call<OAuthToken> call, Response<OAuthToken> response) {
                    nc.setToken(response.body());
                    progres.dismiss();
                }

                @Override
                public void onFailure(Call<OAuthToken> call, Throwable t) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_favorites:
                pager.setCurrentItem(2);
                break;
            case R.id.nav_nearby:
                pager.setCurrentItem(1);
                break;
            case R.id.nav_search:
                pager.setCurrentItem(0);
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void searchTwitter(String query) {
        pager.setCurrentItem(0);
        Fragment f = ((PagerAdapter)pager.getAdapter()).getCurrentFragment();
        if(f instanceof SearchFragment){
            ((SearchFragment) f).requestSearch(query);
        }
    }


}
