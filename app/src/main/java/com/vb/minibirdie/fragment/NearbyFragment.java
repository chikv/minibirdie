package com.vb.minibirdie.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vb.minibirdie.R;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.network.NetworkController;
import com.vb.minibirdie.network.SearchResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyFragment extends BaseTimelineFragment {

    public static final long LOCATION_TIMEOUT = 10L * 1000L;
    private static final int PERMISSIONS_REQUEST_LOCATION = 1;
    private ImageButton refreshButton;
    private TextView locationText;
    private LocationManager locationManager;
    private Location currentLocation = null;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby, null);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshButton = view.findViewById(R.id.refresh);
        locationText = view.findViewById(R.id.location_string);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateLocation();
            }
        });

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentLocation = location;
                refreshButton.setEnabled(true);
                if (cancelTask != null) {
                    cancelTask.cancel();
                    cancelTask = null;
                }
                locationText.setText(getLocationAddress(location));
                if(tweets.size() == 0) {
                    requestItemsFromStart();
                }

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }

    private LocationListener locationListener;
    private Timer locationTimer = new Timer();
    private TimerTask cancelTask;

    private void updateLocation() {
        if (cancelTask != null) {
            cancelTask.cancel();
            cancelTask = null;
        }

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_LOCATION);

            return;
        }

        cancelTask = new TimerTask() {

            @Override
            public void run() {
                //cancel all activities related to location update
                locationManager.removeUpdates(locationListener);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshButton.setEnabled(true);
                        if(currentLocation == null) {
                            locationText.setText(R.string.no_location);
                        }
                    }
                });

            }
        };


        refreshButton.setEnabled(false);
        locationText.setText(R.string.updating_location);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
        locationTimer.schedule(cancelTask, LOCATION_TIMEOUT);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateLocation();
                } else {
                    showAlertMessage(R.string.dialog_title_error, R.string.dialog_message_enable_location_perm);
                }
                break;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        if(isLocationEnabled()){
            Location lastLocation = getLastLocation();
            if(lastLocation != null) {
                locationText.setText(getLocationAddress(lastLocation));
                currentLocation = lastLocation;
                requestItemsFromStart();
            } else {
                updateLocation();
            }
        } else {
            showAlert();
        }


    }

    private String getLocationAddress(Location lastLocation) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getContext(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(addresses != null && addresses.size() > 0){
            StringBuilder builder = new StringBuilder();
            Address address = addresses.get(0);
            String addressString = address.getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = address.getLocality();
            String state = address.getAdminArea();
            String country = address.getCountryName();
            String postalCode = address.getPostalCode();
            String knownName = address.getFeatureName();

            if(!TextUtils.isEmpty(city)){
                builder.append(city).append(", ");
            }
            if(!TextUtils.isEmpty(addressString)){
                builder.append(addressString);
            }

            return builder.toString();
        } else {
            return lastLocation.getLatitude() + ", " + lastLocation.getLongitude();
        }


    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private Location getLastLocation() {
        Context ctx = getContext();
        Location location = null;


        if (ctx != null) {


            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }


            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) {

                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }
        return location;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && currentLocation != null && tweets.size() == 0){
            //init search
            requestItemsFromStart();
        }
    }

    @Override
    public void requestItemsFromStart() {

        if(NetworkController.getInstance().getToken() == null) {
            loadedTop(new ArrayList<Tweet>());
            return;
        }

        Call<SearchResponse> request;
        if(tweets.isEmpty()){
            //initial request
            request = netController.getService().searchNaearby(50, currentLocation.getLatitude() + "," + currentLocation.getLongitude() + ",10km");
        } else {
            request = netController.getService().searchNaearby(50, currentLocation.getLatitude() + "," + currentLocation.getLongitude() + ",10km", tweets.get(0).getId());
        }
        request.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if(response.code() == 200){
                    List<Tweet> newTweets = response.body().getStatuses();
                    loadedTop(newTweets);
                } else {
                    loadedTop(new ArrayList<Tweet>());
                    notifyError();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                loadedTop(new ArrayList<Tweet>());
                notifyError();
            }
        });
    }

    @Override
    protected void requestItemsFromEnd() {
        Call<SearchResponse> request;
        if(tweets.isEmpty()){
            //initial request
            request = netController.getService().searchNaearby(50, currentLocation.getLatitude() + "," + currentLocation.getLongitude() + ",10km");
        } else {
            request = netController.getService().searchNaearbyAfter(50, currentLocation.getLatitude() + "," + currentLocation.getLongitude() + ",10km", tweets.get(tweets.size() - 2).getId());
        }
        request.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if(response.code() == 200){
                    List<Tweet> newTweets = response.body().getStatuses();
                    loadedBottom(newTweets);
                } else {
                    loadedBottom(new ArrayList<Tweet>());
                    notifyError();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                loadedBottom(new ArrayList<Tweet>());
                notifyError();

            }
        });
    }
}
