package com.vb.minibirdie.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vb.minibirdie.BuildConfig;
import com.vb.minibirdie.R;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.network.OAuthToken;
import com.vb.minibirdie.network.SearchResponse;

import com.vb.minibirdie.network.TwitterService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchFragment extends BaseTimelineFragment {

    private TextView searchText;
    private static final String TAG = "SearchFragment";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search,null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        searchText = view.findViewById(R.id.search);

        searchText.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 500; // milliseconds

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                // refresh your list
                                Log.d(TAG, "search for " + s.toString());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        searchTwitter(s.toString());
                                    }
                                });

                            }
                        },
                        DELAY
                );
            }
        });

    }

    private void searchTwitter(String s) {
        if(TextUtils.isEmpty(s)){
            return;
        }
        tweets.clear();
        adapter.notifyDataSetChanged();
        requestItemsFromStart();
    }

    public void requestSearch(String searchTerm) {
        if(TextUtils.isEmpty(searchTerm)){
            return;
        }
        searchText.setText(searchTerm);
    }

    @Override
    public void requestItemsFromStart() {
        Call<SearchResponse> request;

        if(TextUtils.isEmpty(searchText.getText())) {
            return;
        }

        if(tweets.isEmpty()){
            //initial request
            request = netController.getService().search(searchText.getText().toString(), 50);
        } else {
            request = netController.getService().search(searchText.getText().toString(), 50, tweets.get(0).getId());
        }
        request.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if(response.code() == 200){
                    List<Tweet> newTweets = response.body().getStatuses();
                    loadedTop(newTweets);
                } else {
                    loadedTop(new ArrayList<Tweet>());
                    notifyError();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                loadedTop(new ArrayList<Tweet>());
                notifyError();
            }
        });
    }

    @Override
    protected void requestItemsFromEnd() {
        Call<SearchResponse> request;
        if(tweets.isEmpty()){
            //initial request
            request = netController.getService().search(searchText.getText().toString(), 50);
        } else {
            request = netController.getService().searchSince(searchText.getText().toString(), 50,
                    tweets.get(tweets.size() - 2).getId());
        }
        request.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                if(response.code() == 200){
                    List<Tweet> newTweets = response.body().getStatuses();
                    loadedBottom(newTweets);
                } else {
                    loadedBottom(new ArrayList<Tweet>());
                    notifyError();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                loadedBottom(new ArrayList<Tweet>());
                notifyError();
            }
        });

    }




}
