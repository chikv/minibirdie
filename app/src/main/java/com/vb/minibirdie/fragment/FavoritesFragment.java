package com.vb.minibirdie.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vb.minibirdie.R;
import com.vb.minibirdie.db.AppDatabase;
import com.vb.minibirdie.model.Tweet;

import java.util.List;

public class FavoritesFragment extends BaseTimelineFragment {


    private LoaderManager.LoaderCallbacks<List<Tweet>> callback;
    private BroadcastReceiver receiver;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter.setOnLoadMoreListener(null);
        requestItemsFromStart();
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        callback = new LoaderManager.LoaderCallbacks<List<Tweet>>() {
            @NonNull
            @Override
            public Loader<List<Tweet>> onCreateLoader(int id, @Nullable Bundle args) {
                return new TweetDbLoader(context);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<List<Tweet>> loader, List<Tweet> data) {
                loadedTop(data);
            }

            @Override
            public void onLoaderReset(@NonNull Loader<List<Tweet>> loader) {

            }
        };
        getLoaderManager().initLoader(
                LOADER_FAVS, null, callback);
    }

    @Override
    public void onStart() {
        super.onStart();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                requestItemsFromStart();
            }
        };
        broadcastManager.registerReceiver(receiver, new IntentFilter(ACTION_TWEET_UPDATED));
    }

    @Override
    public void onStop() {
        super.onStop();
        broadcastManager.unregisterReceiver(receiver);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favorites,null);
    }


    @Override
    public void requestItemsFromStart() {
        tweets.clear();
        adapter.notifyDataSetChanged();
        //loadedTop(AppDatabase.getInstance().tweetDao().getFavorites());

        Loader<Object> loader = getLoaderManager().getLoader(LOADER_FAVS);
        if(loader == null){
            getLoaderManager().initLoader(LOADER_FAVS, null, callback);
        } else {
            getLoaderManager().restartLoader(LOADER_FAVS, null, callback);
        }
    }

    @Override
    protected void requestItemsFromEnd() {
        //not needed here
    }

    @Override
    protected void unfavoriteTweet(Tweet tweet) {
        super.unfavoriteTweet(tweet);
        tweets.remove(tweet);
        adapter.notifyDataSetChanged();
        //AppDatabase.getInstance().tweetDao().delete(tweet);
    }

    private static class TweetDbLoader extends AsyncTaskLoader<List<Tweet>> {

        public TweetDbLoader(Context context) {
            super(context);
        }

        @Override
        public List<Tweet> loadInBackground() {
            return AppDatabase.getInstance().getFavorites();
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }
    }

}
