package com.vb.minibirdie.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.vb.minibirdie.R;
import com.vb.minibirdie.adapter.OnLoadMoreListener;
import com.vb.minibirdie.adapter.TweetAdapter;
import com.vb.minibirdie.db.AppDatabase;
import com.vb.minibirdie.model.HashtagEntity;
import com.vb.minibirdie.model.MentionEntity;
import com.vb.minibirdie.model.Tweet;
import com.vb.minibirdie.model.TweetEntity;
import com.vb.minibirdie.model.UrlEntity;
import com.vb.minibirdie.network.NetworkController;

import java.util.ArrayList;
import java.util.List;

import static com.vb.minibirdie.adapter.TweetAdapter.ACTION_FAVORITE;
import static com.vb.minibirdie.adapter.TweetAdapter.ACTION_UNFAVORITE;
import static com.vb.minibirdie.adapter.TweetAdapter.EXTRA_TWEET_ID;

public abstract class BaseTimelineFragment extends android.support.v4.app.Fragment{

    public static final String TAG = "BaseTimelineFragment";
    protected SwipeRefreshLayout swipeRefresh;
    protected RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    protected NetworkController netController;

    protected ArrayList<Tweet> tweets = new ArrayList<>();
    protected TweetAdapter adapter;
    protected Handler handler;
    private FragmentInteractionListener mCallback;
    protected LocalBroadcastManager broadcastManager;
    private LoaderManager.LoaderCallbacks<Tweet> twActionCallback;

    public static final int LOADER_FAV_TWEET = 1;
    public static final int LOADER_UNFAV_TWEET = 2;
    public static final int LOADER_FAVS = 3;

    public static final String ACTION_TWEET_UPDATED = "tweet_updated";

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(TextUtils.isEmpty(action)){
                return;
            }
            switch (action){
                case ACTION_FAVORITE:
                case ACTION_UNFAVORITE:
                    AppDatabase.getInstance().applyFavoritesFlag(tweets);
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * called when tweet action happened. Override if you need to do additional work, like
     * loading timeline from db
     * @param tweet
     */
    protected void tweetActionHappened(Tweet tweet) {
        Intent i = new Intent();
        i.setAction(ACTION_TWEET_UPDATED);
        i.putExtra(EXTRA_TWEET_ID, tweet.getId());
        broadcastManager.sendBroadcast(i);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        netController = NetworkController.getInstance();
        handler = new Handler();

        //just init cache for fav id's.
        //no need for separate Loader or simething similar.
        //just moves to BG threrad from main
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                AppDatabase.getInstance().initCache();
                return null;
            }
        }.execute();


    }



    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (FragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

        broadcastManager = LocalBroadcastManager.getInstance(context);

    }

    public void notifyError() {
        showAlertMessage(R.string.timeline_error_title, R.string.timeline_error_message);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        broadcastManager.registerReceiver(mMessageReceiver, new IntentFilter(ACTION_FAVORITE));
        broadcastManager.registerReceiver(mMessageReceiver, new IntentFilter(ACTION_UNFAVORITE));
    }

    @Override
    public void onStop() {
        super.onStop();
        broadcastManager.unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh = view.findViewById(R.id.swiperefresh);
        recyclerView = view.findViewById(R.id.recyclerview);

        if(swipeRefresh == null) {
            throw new IllegalStateException("Layout should contain SwipeRefreshLayout!");
        }

        if(recyclerView == null) {
            throw new IllegalStateException("Layout should contain RecyclerView!");
        }

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                requestItemsFromStart();
            }
        });

        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        adapter = new TweetAdapter(tweets, recyclerView);
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void loadMoreItems() {
                tweets.add(null);
                adapter.notifyItemInserted(tweets.size() - 1);
                requestItemsFromEnd();
            }
        });

        adapter.setTweetInteractionListener(new TweetAdapter.TweetInteractionListener() {
            @Override
            public void entityClick(TweetEntity tweetEntity) {
                if(tweetEntity instanceof MentionEntity) {
                    mCallback.searchTwitter("@" + ((MentionEntity) tweetEntity).getScreenName());
                } else if(tweetEntity instanceof HashtagEntity) {
                    mCallback.searchTwitter("#" + ((HashtagEntity) tweetEntity).getText());
                } else if(tweetEntity instanceof UrlEntity) {
                    //launch browser?
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(((UrlEntity) tweetEntity).getUrl()));
                    startActivity(intent);
                }

            }

            @Override
            public void favoriteTweet(Tweet tweet) {
                BaseTimelineFragment.this.favoriteTweet(tweet);
            }

            @Override
            public void unfavoriteTweet(Tweet tweet) {
                BaseTimelineFragment.this.unfavoriteTweet(tweet);
            }
        });


        recyclerView.setAdapter(adapter);

    }
    


    public abstract void requestItemsFromStart();

    protected abstract void requestItemsFromEnd();

    public void loadedTop(List<Tweet> newTweets){
        if(tweets != null) {
            AppDatabase.getInstance().applyFavoritesFlag(newTweets);
            tweets.addAll(0, newTweets);
            adapter.notifyDataSetChanged();
        }
        swipeRefresh.setRefreshing(false);
    }

    public void loadedBottom(List<Tweet> newTweets){
        AppDatabase.getInstance().applyFavoritesFlag(newTweets);
        tweets.remove(tweets.size() - 1);
        adapter.notifyItemRemoved(tweets.size() - 1);
        tweets.addAll(newTweets);
        adapter.setLoaded();
        adapter.notifyDataSetChanged();
    }

    protected void favoriteTweet(final Tweet tweet) {
        LoaderManager.LoaderCallbacks<Tweet> cb = new LoaderManager.LoaderCallbacks<Tweet>() {
            @NonNull
            @Override
            public Loader<Tweet> onCreateLoader(int id, @Nullable Bundle args) {
                return new TweetDbAction(getContext(), DbActions.FAVORITE, tweet);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<Tweet> loader, Tweet data) {
                if(tweets.contains(data)){
                    data.setFavorite(true);
                    adapter.notifyDataSetChanged();
                }
                tweetActionHappened(data);

            }

            @Override
            public void onLoaderReset(@NonNull Loader<Tweet> loader) {

            }
        };
        startActionLoader(cb, DbActions.FAVORITE);

        
    }
    
    

    protected void unfavoriteTweet(final Tweet tweet) {
        LoaderManager.LoaderCallbacks<Tweet> cb = new LoaderManager.LoaderCallbacks<Tweet>() {
            @NonNull
            @Override
            public Loader<Tweet> onCreateLoader(int id, @Nullable Bundle args) {
                return new TweetDbAction(getContext(), DbActions.UNFAVORITE, tweet);
            }

            @Override
            public void onLoadFinished(@NonNull Loader<Tweet> loader, Tweet data) {
                if(tweets.contains(data)){
                    data.setFavorite(false);
                    adapter.notifyDataSetChanged();
                }
                tweetActionHappened(tweet);
            }

            @Override
            public void onLoaderReset(@NonNull Loader<Tweet> loader) {

            }
        };
        startActionLoader(cb, DbActions.UNFAVORITE);

    }

    private void startActionLoader(LoaderManager.LoaderCallbacks<Tweet> cb, DbActions action){
        Loader<Tweet> loader;
        switch (action){
            case FAVORITE:
                loader = getLoaderManager().getLoader(LOADER_FAV_TWEET);
                if(loader == null){
                    getLoaderManager().initLoader(LOADER_FAV_TWEET, null, cb);
                } else {
                    getLoaderManager().restartLoader(LOADER_FAV_TWEET, null, cb);
                }
                break;
            case UNFAVORITE:
                loader = getLoaderManager().getLoader(LOADER_UNFAV_TWEET);
                if(loader == null){
                    getLoaderManager().initLoader(LOADER_UNFAV_TWEET, null, cb);
                } else {
                    getLoaderManager().restartLoader(LOADER_UNFAV_TWEET, null, cb);
                }
                break;
        }



    }

    public enum DbActions {
        FAVORITE, UNFAVORITE
    }

    protected static class TweetDbAction extends AsyncTaskLoader<Tweet> {

        private  DbActions action;
        private Tweet tweet;

        public TweetDbAction(Context context, DbActions action, Tweet tweet) {
            super(context);
            this.action = action;
            this.tweet = tweet;
        }

        @Override
        public Tweet loadInBackground() {
            switch (action){
                case FAVORITE:
                    AppDatabase.getInstance().favoriteTweet(tweet);
                    break;
                case UNFAVORITE:
                    AppDatabase.getInstance().unfavoriteTweet(tweet);
                    break;
            }
            return tweet;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }
    }

    public void showAlertMessage(@StringRes int titleId, @StringRes int messageId){
        Context ctx = getContext();
        if(ctx == null) {
            Log.d(TAG, "Can't show message brecause there is no context");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);

        builder.setTitle(titleId).setMessage(messageId);
        builder.setPositiveButton(android.R.string.ok, null);

        builder.create().show();

    }


}
