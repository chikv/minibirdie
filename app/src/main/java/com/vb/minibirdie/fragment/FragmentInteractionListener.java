package com.vb.minibirdie.fragment;

public interface FragmentInteractionListener {
    void searchTwitter(String query);
}